﻿using System;

namespace Lab_3_Task_3
{
    class Task_3
    {
        static void Main(string[] args)
        {
            //Комманда ,которая просит полозвателя написать название шахматной фигуры
            Console.WriteLine("Введите название шахматной фигуры");
            //Комманда ,которая считывает тип данных string
            string chessPieceName = Console.ReadLine().ToLower();
            //Начало конструкции switch ,которая сравнивает несколько условий
            switch (chessPieceName)
            {
                case "король":
                case "королева":
                case "ладья":           //Эти условия объединены в один так как у них один и тот же вывод
                case "слон":
                case "конь":
                    Console.WriteLine("Эта фигура находится на горизонтали 1 и 8");
                    break;

                case "пешка":
                    Console.WriteLine("Эта фигура находится на горизонтали 2 и 7");
                    break;

                default:
                    Console.WriteLine("Такой фигуры нет ");
                    break;
            }
        }
    }
}
