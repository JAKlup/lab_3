﻿using System;

namespace Lab_3
{
    class Task_1
    {
        static void Main(string[] args)
        {
            //Комманда ,которая говорит ползователю ввести значение переменной x
            Console.WriteLine("Введите значение переменной x,где x > 300");
            //Комманда ,которая ждёт от ползователя ввода строки
            string xstr = Console.ReadLine();
            //Комманда ,которая конвертирует строковы тип в тип double
            double x = Convert.ToDouble(xstr);
            //Объявляется условие 
            if (x > 300)
            {
                double y = 13 * x - 2;

                Console.WriteLine("Значение переменной y = " + y);
            }
            else if (x >= 0 && x <= 300) //Используется второе условие , если первое условие не подошло
            {
                double y2 = 1 + 12 * x;

                Console.WriteLine("Значение переменной y = " + y2);
            }
            else //Используется третье условие ,если предыдущие не подошли
            {
                double y = 10 * x;

                Console.WriteLine("Значение переменной y = " + y);
            }
        }
    }
}
