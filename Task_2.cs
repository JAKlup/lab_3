﻿using System;

namespace Lab_3_Task_2
{
    class Task_2
    {
        static void Main(string[] args)
        {
            //Объявляется функцмя которая будет проверять булевые переменны x,y,z
            static void Сheck(bool x, bool y, bool z)
            {   //Блок кода ,в котором условие проверки
                bool f = !z | !x | !x & y;
                if (f == false)
                {
                    Console.WriteLine("x - {0}, y - {1}, z - {2}", x, y, z);
                }
            }
            //Возможные варианты для проверки
            Сheck(false, false, false);
            Сheck(false, false, true);
            Сheck(false, true, false);
            Сheck(false, true, true);
            Сheck(true, false, false);
            Сheck(true, false, true);
            Сheck(true, true, false);
            Сheck(true, true, true);

            Console.ReadKey();
        }
    }
}
