﻿using System;

namespace Lab_3_Task_4
{
    class Task_4
    {
        static void Main(string[] args)
        {
            int age = Convert.ToInt32(Console.ReadLine());
            if (age < 1 || age > 99)
            {
                Console.WriteLine("Данный возраст не входит в зпдпнный промежуток");
                return;
            }

            int firstDigit = age / 10;
            int secondDigit = age % 10;

            if (secondDigit == 1 && firstDigit != 1)
            {
                Console.WriteLine("Мне {0} год", age);
            }
            else if (secondDigit >= 2 && secondDigit <= 4 && firstDigit != 1)
            {
                Console.WriteLine("Мне {0} года", age);
            }
            else
            {
                Console.WriteLine("Мне {0} лет", age);
            }

        }
    }
}
